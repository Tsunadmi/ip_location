# Face analyzer

IP location App


## Installation

You need install python 3.9:

```
pip install python
```

## Important import
```
import folium

```

## Getting started

App strat:

```
python Proga.py
```

## Technologies used

- Python
- JetBrains
- API


## Description

App allows indicate location via IP address.


## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)

