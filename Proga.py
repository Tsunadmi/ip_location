import requests
import folium

def get_info_by_ip(ip='127.0.0.1'):
    try:
        respone = requests.get(url=f'http://ip-api.com/json/{ip}').json()
        # print(respone)
        data = {'[IP]': respone.get('query'),
                '[Country]': respone.get('country'),
                '[City]': respone.get('city'),
                '[Zip]': respone.get('zip'),
                '[Lat]': respone.get('lat'),
                '[Lon]': respone.get('lon')
        }

        for k,v in data.items():
            print(f'{k} : {v}')
        area = folium.Map(location=[respone.get('lat'),respone.get('lon')])
        area.save(f'{respone.get("query")}_{respone.get("country")}.html')

    except requests.exceptions.ConnectionError:
        print('Some problem')

def main():
    ip = input('Enter IP adress: ')
    get_info_by_ip(ip=ip)

if __name__=='__main__':
    main()